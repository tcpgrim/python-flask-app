FROM python:3.11.3-slim
RUN apt update -y && apt install -y curl libpq-dev gcc
WORKDIR /app
COPY . /app
RUN pip install -r requirements && rm requirements
EXPOSE 80
HEALTHCHECK --interval=30s --timeout=30s --start-period=30s --retries=5 \
    CMD curl -f http://127.0.0.1:80/health || exit 1
ENTRYPOINT ["python", "-m", "flask", "run", "--host=0.0.0.0", "--port=80"]