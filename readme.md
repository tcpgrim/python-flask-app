Python flask app with PostgreSQL integration
------
#### I made this project to train my DevOPS and programming skills. The main objective of this project is to create a python app, containerize with Dockerfile and integrate it with PostgreSQL. And lastly automate it with docker-compose.


Python version used: 3.11.3
PostgreSQL version used: 15.3


Setup:
Clone this project and enter open the folder
```
git pull https://gitlab.com/tcpgrim/python-flask-app.git
cd python-flask-app
```
Important to mention that default location of PostgreSQL database is mapped to your home folder. So default path of database is /home/${USER}/postgresql-db
Simple run docker-compose command and check if everything okay
```
docker-compose up -d
```
Check if everything okayt with docker ps command
```
docker ps
```
![example](https://i.imgur.com/7l9omI2.png)

Open app in your browser. Default port is 5000
```
localhost:5000
```