from datetime import datetime
import logging
from flask import Flask, render_template, request, redirect, jsonify
from flask_sqlalchemy import SQLAlchemy


# logging configuration
logging.basicConfig(
    level=logging.DEBUG,
    format="<%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s>"
    )


# database configuration
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://admin:pass@postgresql-db:5432/postgresql-db'
db = SQLAlchemy(app)
    


# article class for database
class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    intro = db.Column(db.String(300), nullable=False)
    text = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    def __repr__(self):
        return '<Article %r>' % self.id
    
with app.app_context():
    db.create_all()
    

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/create-article', methods=['POST', 'GET'])
def create_article():
    if request.method == "POST":
        # logging.debug(request.method)
        title = request.form['title']
        intro = request.form['inrto']
        text = request.form['text']
        article = Article(title=title, intro=intro, text=text)
        logging.debug('This is debug msg')
        logging.debug(type(article))
        try:
            db.session.add(article)
            db.session.commit()
            return redirect('/posts')
        except:
            return "При добавлении статьи произошла ошибка"
    else:
        return render_template("create-article.html")


@app.route('/posts')
def posts():
    articles = Article.query.order_by(Article.date.desc()).all()
    return render_template("posts.html", articles=articles)


@app.route('/posts/<int:id>')
def post_info(id):
    article = Article.query.get(id)
    return render_template("post_info.html", article=article)


@app.route('/user/<string:name>/<int:userid>')
def user(name, userid):
    return "User page: " + name + " - " + str(userid)


# health check
@app.route('/health')
def health():
    return jsonify(
        status='UP'
    )


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)